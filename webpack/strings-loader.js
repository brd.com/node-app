var i18nStringsFiles = require('../lib/i18n-strings-files');

module.exports = function(content) {
	this.cacheable && this.cacheable();
  var ret = content;
  if(!content.match(/^module/)) {
    var output = i18nStringsFiles.parse(content);
	  ret = "module.exports = " + JSON.stringify(output)+';';
  }
  return ret;
}
