const path = require('path')
const express = require('express')
const chokidar = require('chokidar')
const middleware = require('../lib/middleware');
const Events = require('../lib/events');

function debounce(time,fn) {
  let timeout;
  return () => {
    clearTimeout(timeout);
    timeout = setTimeout(fn,time);
  }
}

module.exports = function(options) {
  const app = express()

  // Options breakdown:

  // The current directory (only used to create defaults for
  // options.file and options.watch)
  const cwdPath = options && options.srcDir || process.cwd();

  // options.file: the file to load as a server:
  const routesPath = options.file;

  // options.watch: the files or directories to watch for changes (and
  // to remove when they do change):
  const watchPaths = options.watch || [cwdPath];

  const loadIt = debounce(250,() => {
    console.log("Loading app after change");
    require(routesPath)
  });
  
  middleware.before(app,{ development: false });
  
  app.use(process.env.APP_PATH||'/',(req, res, next) => {
    try {
      var routes = require(routesPath);
      (routes.default || routes)(req, res, next)
    } catch(x) {
      console.log("Error in routes file: ",x);
      res.status(500).send(x.toString()+'\n'+x.stack);
    }
  })

  middleware.after(app,{ development: false });
  
  const chokidar = require('chokidar')
  const serverPath = path.join(cwdPath,'server');

  const watcher = chokidar
        .watch(watchPaths)
        .on('change', (path) => {
          Events.emit('shutdown');

          const keys = Object.keys(require.cache)
                .filter(k => watchPaths.find((p) => k.includes(p)))
          keys.forEach(k => delete require.cache[k])
          

          
          console.log("Change registered, foo.");
          loadIt();
        })

  loadIt();

  app.listen(options.port,options.host,() => {
    console.log(`Listening in DEV on ${options.host}:${options.port}`);
  });
}
