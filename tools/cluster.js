const tracer = require('../tracer');

const cluster = require('cluster');
const path = require('path');
const GracefulShutdownManager = require('../lib/graceful-shutdown-manager');
const express = require('express');
const middleware = require('../lib/middleware');

const promBundle = require("express-prom-bundle");

const watchdog = {}
const staleWorkers = [];
const killedWorkers = [];

module.exports = {
  start ({ file, dev, workers, workerMaxRequests, workerMaxLifetime, address, port, rootDir }) {
    if (cluster.isMaster) {
      console.log(`master ${process.pid} is running`);

      // Fork workers
      const numWorkers = workers || require('os').cpus().length
      for (let i = 0; i < numWorkers; i++) {
        cluster.fork();
      }

      const metricsApp = express();
      metricsApp.use('/metrics', promBundle.clusterMetrics());
      metricsApp.listen(9999);
      console.log('cluster metrics listening on 9999');

      if (workerMaxRequests || workerMaxLifetime) {
        cluster.on('message', (worker, msg) => {
          if (msg.cmd) {
            if (msg.cmd === 'notifyRequest') {
              let killWorker = false;
              let message;
              
              if (workerMaxRequests) {
                watchdog[worker.process.pid].req_count++;

                killWorker = watchdog[worker.process.pid].req_count >= workerMaxRequests;
                if (killWorker) {
                  message = `worker ${worker.process.pid} processed ${watchdog[worker.process.pid].req_count} requests, enqued for deletion`;
                }
              }

              if (!killWorker && workerMaxLifetime) {
                const lifetimeSeconds = process.hrtime(watchdog[worker.process.pid].start)[0];

                killWorker = lifetimeSeconds > workerMaxLifetime;
                if (killWorker) {
                  message = `worker ${worker.process.pid} has been up for ${lifetimeSeconds}s, enqueued for deletion`
                }
              }

              if (killWorker) {
                // fake SIGHUP message to gracefully shutdown the worker
                if(!staleWorkers.includes(worker) && !killedWorkers.includes(worker.process.pid)) {
                  staleWorkers.push(worker);
                  if(message)
                    console.log(message);
                } 
              }

              if(Object.keys(watchdog).length - staleWorkers.length < workers) {
                cluster.fork();
              }
            }
          }
        });
      }

      cluster.on('fork', (worker) => {
        watchdog[worker.process.pid] = { start: process.hrtime(), req_count: 0 };
        console.log(`Worker ${worker.process.pid} forked. ${Object.keys(watchdog).length} workers total`);
      });

      cluster.on('listening', (worker, address) => {
        console.log(`worker ${worker.process.pid} started listening on ${address.address}:${address.port}`);
        var w;
        while(w = staleWorkers.pop()) {
          killedWorkers.push(w.process.pid);
          
          w.send('SIGHUP');
        }
      });

      cluster.on('exit', (worker, code, signal) => {
        const workerLifetime = process.hrtime(watchdog[worker.process.pid].start)
        const msWorkerLifetime = Math.round((workerLifetime[0] * 1E9 + workerLifetime[1]) / 1E6)
        
        let message = `worker ${worker.process.pid} died after ${msWorkerLifetime}ms`;
        if (signal) {
          message += ` by signal: ${signal}`;
        } else if (code !== 0) {
          message += ` with error code: ${code}`;
        }

        if (signal && signal !== 'SIGHUP') {
          console.log(message);
          // if it shut down not because SIGHUP, we need to restart in
          // a hurry!
          cluster.fork();
        }

        // cleanup watchdog
        delete watchdog[worker.process.pid]
      });
    } else {
      
      const ROOT_DIR = rootDir || process.env.ROOT_DIR || process.cwd();

      const worker = express();
      if (workerMaxRequests || workerMaxLifetime) {
        worker.use((req, res, next) => {
          if (cluster.worker.isConnected()) {
            process.send({ cmd: 'notifyRequest' });
          }
          next();
        });
      }

      
      let app = require(file);
      let path = process.env.APP_PATH || '/';

      worker.use(promBundle({ autoregister: false, includeMethod: true }));
      
      // do the right thing with nuxt config files:
      if(typeof app == 'object') {
        const { Nuxt } = require('nuxt');
        worker.use((new Nuxt(app)).render);
      } else {
        middleware.before(worker,{ development: false });
        worker.use(path,app);
        middleware.after(worker,{ development: false });
      }
      
      const server = worker.listen(port, address || '0.0.0.0');
      const shutdownManager = new GracefulShutdownManager(server);

      // shutdown worker gracefully
      let isClosing = false;
      let forceTimeout;
      process.on('message', (msg) => {
        if (msg === 'SIGHUP') {
          if (!isClosing) {
            isClosing = true
            
            shutdownManager.terminate(() => {
              console.log("Server is gracefully terminated");
              process.exit(3);
            });
          }

          // force shutdown after 60s.  No reason to rush, really -
          // this is a memory management courtesy, not time to close
          // down the whole app:
          clearTimeout(forceTimeout);
          forceTimeout = setTimeout(() => {
            console.log("Force exiting");
            process.exit(3);
          }, 60000);
        }
      });

    }
  }
}
