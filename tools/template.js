const fs = require('fs');
const path = require('path');

function templateFile(from,to,replacements) {
  var contents = fs.readFileSync(from).toString('utf-8');

  for(var k in replacements) {
    var v = replacements[k];

    // trick to replace non-regex globally:
    contents = contents.split(k).join(v);
  }

  var stats = fs.statSync(from);
  if(fs.existsSync(to)) { fs.unlinkSync(to); }
  fs.writeFileSync(to,Buffer.from(contents,'utf-8'),{ mode: stats.mode });
}

function templateDirectory(from,to,replacements) {
  if (!fs.existsSync(to)){
    fs.mkdirSync(to);
  }

  var files = fs.readdirSync(from);

  for(var i in files) {
    var f = path.resolve(from,files[i]);
    var t =  path.resolve(to,files[i]);
    
    template(f,t,replacements);
  }
}

function template(from,to,replacements) {
  var stats = fs.lstatSync(from);

  if(stats.isFile()) {
    templateFile(from,to,replacements);
  } else if(stats.isDirectory()) {
    templateDirectory(from,to,replacements);
  }
}

module.exports = template;
