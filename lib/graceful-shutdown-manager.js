function GracefulShutdownManager(server) {
  this.server = server;
  this.connections = {};
  this.nextConnectionId = 1;
  this.terminating = false;
  this.startWatchingServer();
}
GracefulShutdownManager.prototype.terminate = function (callback) {
  this.terminating = true;
  this.server.close(callback);
  for (var connectionId in this.connections) {
    if (this.connections.hasOwnProperty(connectionId)) {
      var socket = this.connections[connectionId];
      this.closeIdleConnection(socket);
    }
  }
};
GracefulShutdownManager.prototype.startWatchingServer = function () {
  this.server.on('connection', this.onConnection.bind(this));
  this.server.on('request', this.onRequest.bind(this));
};
GracefulShutdownManager.prototype.onConnection = function (connection) {
  var _this = this;
  var connectionId = this.nextConnectionId++;
  connection.$$isIdle = true;
  this.connections[connectionId] = connection;
  connection.on('close', function () { return delete _this.connections[connectionId]; });
};
GracefulShutdownManager.prototype.onRequest = function (request, response) {
  var _this = this;
  var connection = request.connection;
  connection.$$isIdle = false;
  response.on('finish', function () {
    connection.$$isIdle = true;
    if (_this.terminating) {
      _this.closeIdleConnection(connection);
    }
  });
};
GracefulShutdownManager.prototype.closeIdleConnection = function (connection) {
  if (connection.$$isIdle) {
    connection.destroy();
  }
};

module.exports = GracefulShutdownManager;
