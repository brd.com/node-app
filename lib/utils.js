const union = (...arr) => Array.prototype.concat.call(...arr).filter((o,i,a) => a.indexOf(o) === i);

function deepMerge(to,from) {
  if(!to) {
    return from;
  }
  if(typeof to != 'object' || Array.isArray(to)) {
    return from || to;
  }
  
  var r = {};
  var keys = union(Object.keys(from||{}),Object.keys(to||{}));

  for(var i in keys) {
    var k = keys[i];

    var v1 = to && to[k];
    var v2 = from && from[k];

    r[k] = deepMerge(v1,v2);
  }
  
  return r;
}


module.exports = {
  get: function(object,path) {
    let list;
    
    if(arguments.length > 2) {
      list = Array.prototype.slice.call(arguments,1)
    } else {
      list = path.split('.');
    }
    
    return list.reduce((o,k) => o && o[k],object);
  },
  deepMerge: deepMerge,
}
