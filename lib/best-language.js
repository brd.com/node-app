function parse(preference) {
  if(typeof preference == 'string') {
    return preference.split(';')[0].split(',');
  }

  return preference;
};

function lang(locale) {
  var m = locale.match(/^([^-_]+)(?:(?:-|_).*)?$/);

  if (m) { return m[1]; }
  return locale;
};

module.exports = function(preference, options, defaultLanguage) {
  var preferences = parse(preference);
  var backup = defaultLanguage;

  for(var i in preferences) {
    var p = preferences[i];

    // compare fully:
    var choice = options.find((option) => (option).toLowerCase() == p);

    if (choice) {
      return choice;
    }

    // compare just language (not region)
    choice = options.find((option) => lang(option) == lang(p));
    if (choice) {
      return choice;
    }
  }

  return backup || options[0];
};
