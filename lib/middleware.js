require('./express-async-errors');

let Log;
const { Console }  = require('console');
const i18n = require('../i18n');

const rootConsole = new Console({ stdout: process.stderr, stderr: process.stderr });

try {
  Log = require('@brd.com/log');
} catch(x) {
  console.warn("BRD-Log not available.  req.console won't exist,",x);
}

const requestId = function() {
  const set = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const random = (set) => set[Math.floor(Math.random()*set.length)];
  return 'LLLLLL'.replace(/L/g,() => random(set));
}

let Sentry;
const LOG_MIDDLEWARE = [
  function(next,options,...args) {
    if(options.Sentry) {
      if(typeof args[0] == 'object') {
        options.Sentry.addBreadcrumb({
          level: options.level,
          data: args[0],
        });
      } else if(typeof args[0] == 'string') {
        let data = undefined;
        if(args.length > 1) {
          data = { rest: args.slice(1) }
        }

        options.Sentry.addBreadcrumb({
          category: 'console',
          level: options.level,
          message: args[0],
          data: data,
        });
      } else {
        options.Sentry.addBreadcrumb({
          category: 'console',
          level: options.level,
          data: { args }
        });
      }
    }

    var message = args[0].toString();
    var rest = args.slice(1);

    rootConsole[options.level].call(rootConsole,`[${options.req.id}] ${message}`,...rest);
  },
];

module.exports = {
  // install relevant middleware - i.e. sentry, logging, etc.
  before: function(app,options={}) {
    var dev = 'development' in options && options.development || true;

    try {
      if(process.env.SENTRY_DSN) {
        console.log("Initializing sentry with ",process.env.SENTRY_DSN);

        Sentry = require('@sentry/node');
        Sentry.init({
          dsn: process.env.SENTRY_DSN,
          environment: process.env.SENTRY_ENV,
          release: process.env.SENTRY_VERSION,
          attachStacktrace: true,
        });

        app.use(Sentry.Handlers.requestHandler());
        app.use((req,res,next) => { req.Sentry = Sentry; next() });
      }
    } catch(x) {
      console.error("ERROR Initializing Sentry");
      console.error(x);
      console.error("Ensure you 'npm i --save @sentry/node'.");
    }

    app.use(async (req,res,next) => {
      var locale = req.get('accept-language');

      // force loading every request if we're in development:
      await i18n.load(locale,process.env.NODE_ENV=='development');

      req.$t = function(key,substitutions) {
        return i18n.translate(locale,key,substitutions);
      };

      next();
    });

    app.use((req,res,next) => {
      req.id = req.id || req.get('cf-ray') || requestId();

      if(Sentry) {
        Sentry.configureScope((scope) => {
          scope.setTag("requestid", req.id);
        });
      }

      rootConsole.log(`[${req.id}] [${process.env.SENTRY_ENV || "dev"}] ${req.method} ${req.url}`);

      next();
    });


    if(Log) {
      app.use((req,res,next) => {
        req.console = Log({ req,res,Sentry:req.Sentry  });
        req.console.middleware = LOG_MIDDLEWARE;

        next();
      });
    }
  },
  after: function(app,options={}) {
    app.use(function(err,req,res,next) {
      rootConsole.error(`[${req.id}] `,err);

      if(Sentry) {
        try {
          Sentry.captureException(err);
        } catch(x) {
          rootConsole.error(`[${req.id}] Exception while reporting exception: `,x);
        }
      }

      var error = 'Internal Server Error';
      var stack = '';

      if(process.env.NODE_ENV != 'production') {
        error = String(err)
        stack = err.stack;
      }

      const accept = req.get('accept');

      if(accept && accept.match(/^application\/json/)) {
        res.status(500).json({
          requestId: req.id,
          error,
          stack,
        });
      } else {
        res.status(500).send(`500: ${error}\n\n${stack.split('\n').join('<br/>')}`);
      }
    });
  }
}
