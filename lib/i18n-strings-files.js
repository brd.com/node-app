function i18nStringsFiles() {};

i18nStringsFiles.prototype.parse = function(input, wantsComments) {
  var currentComment, lines, nextLineIsComment, reAssign, reCommentEnd, reLineEnd, result;
  if (!wantsComments) {
    wantsComments = false;
  }
  reAssign = /[^\\]" = "/;
  reLineEnd = /";$/;
  reCommentEnd = /\*\/$/;
  result = {};
  lines = input.split("\n");
  currentComment = '';
  nextLineIsComment = false;
  lines.forEach(function(line) {
    var msgid, msgstr, val;
    line = line.trim();
    line = line.replace(/([^\\])("\s*=\s*")/g, "$1\" = \"");
    line = line.replace(/"\s+;/g, '";');
    if (nextLineIsComment) {
      if (line.search(reCommentEnd) === -1) {
        currentComment += '\n' + line.trim();
      } else {
        nextLineIsComment = false;
        currentComment += '\n' + line.substr(0, line.search(reCommentEnd)).trim();
      }
    } else if (line.substr(0, 2) === '/*') {
      if (line.search(reCommentEnd) === -1) {
        nextLineIsComment = true;
        currentComment = line.substr(2).trim();
      } else {
        nextLineIsComment = false;
        currentComment = line.substr(2, line.search(reCommentEnd) - 2).trim();
      }
    }
    if (line.substr(0, 1) !== '"' || line.search(reAssign) === -1 || line.search(reLineEnd) === -1) {
      return;
    }
    msgid = line;
    msgid = msgid.substr(1);
    msgid = msgid.substr(0, msgid.search(reAssign) + 1);
    msgstr = line;
    msgstr = msgstr.substr(msgstr.search(reAssign) + 6);
    msgstr = msgstr.substr(0, msgstr.search(reLineEnd));
    msgid = msgid.replace(/\\"/g, "\"");
    msgstr = msgstr.replace(/\\"/g, "\"");
    msgid = msgid.replace(/\\n/g, "\n");
    msgstr = msgstr.replace(/\\n/g, "\n");
    if (!wantsComments) {
      return result[msgid] = msgstr;
    } else {
      val = {
        'text': msgstr
      };
      if (currentComment) {
        val['comment'] = currentComment;
        currentComment = '';
      }
      return result[msgid] = val;
    }
  });
  return result;
};

i18nStringsFiles.prototype.compile = function(data, wantsComments) {
  var comment, msgid, msgstr, output, val;
  if (!wantsComments) {
    wantsComments = false;
  }
  if (typeof data !== "object") {
    return "";
  }
  output = "";
  for (msgid in data) {
    val = data[msgid];
    msgstr = '';
    comment = null;
    if (typeof val === 'string') {
      msgstr = val;
    } else {
      if (val.hasOwnProperty('text')) {
        msgstr = val['text'];
      }
      if (wantsComments && val.hasOwnProperty('comment')) {
        comment = val['comment'];
      }
    }
    msgid = msgid.replace(/"/g, "\\\"");
    msgstr = msgstr.replace(/"/g, "\\\"");
    msgid = msgid.replace(/\n/g, "\\n");
    msgstr = msgstr.replace(/\r?\n/g, "\\n");
    if (comment) {
      output = output + "/* " + comment + " */\n";
    }
    output = output + "\"" + msgid + "\" = \"" + msgstr + "\";\n";
  }
  return output;
};

module.exports = new i18nStringsFiles();
