// the straight-node way of interfacing with i18n

const path = require('path');
const fs = require('fs');
const util = require ('util');
const { get } = require('./lib/utils');

const stringsFile = require('./lib/i18n-strings-files');

const folderPath = path.resolve(process.cwd(),'i18n/');

const readFile = util.promisify(fs.readFile);
const readdir = util.promisify(fs.readdir);

const substitute = function(string,args) {
  return string.replace(/{\s*(\S+?)\s*}/g,function(match,key) { return args && args[key]||''; })
};

const defaultLocale = process.env.DEFAULT_LOCALE || 'en';
let locales = null;
const localeCache = {};

const i18n = module.exports = {
  bestLanguage: require('./lib/best-language'),
  getSupportedLocaleFiles: async () => {
    
    const localeExtension = /(\.strings)/;
    
    let files = [];

    if(fs.existsSync(folderPath)) {
      files = await readdir(folderPath);
    }


    // Determine what supported Locales are in the locales folder
    const supportedLocales = [];
    for (const file of files) {
      if (!file.match(localeExtension)) continue;
      supportedLocales.push(file.replace(localeExtension, ''));
    }

    return supportedLocales;
  },
  getStrings: async (locale) => {
    if(!locale) return {};
    const p = path.join(folderPath,`${locale}.strings`);
    var string = await readFile(p,'utf-8');
    return stringsFile.parse(string);
  },
  load: async (locale,force) => {
    locales = locales || await i18n.getSupportedLocaleFiles();
    var choice = i18n.bestLanguage(locale,locales,defaultLocale);
    if(!force && choice in localeCache) return;
    return localeCache[choice] = await i18n.getStrings(choice);
  },
  translate(locale,key,substitutions) {
    var choice = i18n.bestLanguage(locale,locales,defaultLocale);
    let result = get(localeCache,choice,key) || get(localeCache,defaultLocale,key) || key;
    return substitute(result,substitutions);
  }
}
