const locales = require.context('@/i18n/',false,/.strings$/,'lazy');

const getSupportedLocaleFiles = async () => {
  // Determine what supported Locales are in the locales folder
  const supportedLocales = [];
  for (const folder of locales.keys()) {
    var m = folder.match(/^\.\/(.*?)\.strings$/);
    
    if(m) {
      supportedLocales.push(m[1]);
    }
  }

  return supportedLocales;
};

const importLocaleFile = async (locale) => {
  return await locales(`./${locale}.strings`).then(l => l.default || l);
};

export const namespaced = true;

export const state = () => ({
  currentLocale: 'en',
  supportedLocales: ['en'],
  locales: {},
});

export const mutations = {
  SET_LANG(state, localeName) {
    state.currentLocale = localeName;
  },
  SET_LOCALE(state, { currentLocale, localeData }) {
    state.locales[currentLocale] = localeData;
  },
  SET_SUPPORTED_LOCALES(state, supportedLocales) {
    state.supportedLocales = supportedLocales;
  },
}

export const actions = {
  async preload({ commit, state }, hash) {
    const { currentLocale } = state || {};

    // Only send the current locale when populating the locales store, but make an index of the locales we do support for automatic locale selection based on the user's locale. We'll fetch any locales aside from the fallback locale that don't exist in the store dynamically from the file system and place it in the store's cache based on their locale at runtime.
    const localeData = await importLocaleFile(currentLocale);
    commit('SET_LOCALE', { currentLocale, localeData });

    const supportedLocales = await getSupportedLocaleFiles();
    commit('SET_SUPPORTED_LOCALES', supportedLocales);

    return state.supportedLocales;
  },
  async getLocales({ commit, state }, hash) {
    const { currentLocale, locales } = state;

    // Fetch from filesystem if the current locale isn't stored in vuex store
    if (!locales[currentLocale]) {
      console.log(`No locale messages found in cache for locale ${currentLocale}! Fetching from file system and storing in vuex store cache.`);
      const localeData = await importLocaleFile(currentLocale);
      commit('SET_LOCALE', { currentLocale, localeData });
      return locales;
    }

    return locales;
  },
}
