const path = require('path')
const middleware = require('../lib/middleware');

module.exports = function ServerModule (moduleOptions) {
  const cwdPath = this.options.srcDir

  const options = {
    serverPath: 'server',
    routesPath: 'server/index.js',
    ...moduleOptions
  }

  const routesPath = path.join(cwdPath, options.routesPath)

  const express = require('express')

  // handle hot loading of routes
  // re-initializes everything when file changes inside server dir
  if (this.options.dev) {
    const chokidar = require('chokidar')

    const app = express()

    middleware.before(app,{development: true});
    app.use((req, res, next) => {
      try {
        var routes = require(routesPath);
        (routes.default || routes)(req, res, next)
      } catch(x) {
        res.status(500).send(x.toString()+'\n'+x.stack);
      }
    })
    middleware.after(app,{development: true});

    const serverPath = path.join(cwdPath, options.serverPath)
    const libPath = path.join(cwdPath,'lib');
    
    const watchPaths = [serverPath,libPath]

    const watcher = chokidar
          .watch(watchPaths)
          .on('change', (path) => {
            const keys = Object.keys(require.cache)
                  .filter(k => watchPaths.find(p => k.includes(p)))
        keys.forEach(k => delete require.cache[k])
      })

    this.nuxt.hook('close', () => {
      watcher.close()
    })

    this.addServerMiddleware(app)
  }  else {
    const app = express();

    middleware.before(app,{development: false });

    var routes = require(routesPath);
    app.use(routes.default || routes);

    middleware.after(app,{development: false });
    
    this.addServerMiddleware(app);
  }

  this.extendBuild((config, { isClient, isServer }) => {
    config.module.rules.unshift({
      test: /\.strings$/,
      loader: 'strings-loader',
    })

    const path = require('path');
    config.resolveLoader.modules.push(path.resolve(__dirname, '../webpack'));
  });

  this.addPlugin(path.resolve(__dirname,'./plugins/i18n.js'));
  
  // this.nuxt.store.registerModule('i18n',path.resolve(__dirname,'./store/i18n.js'),{ preserveState: true });
}
