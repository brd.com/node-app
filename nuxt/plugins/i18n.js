import Vue from 'vue';
import VueI18n from 'vue-i18n';
import * as i18nModule from '@brd.com/node-app/nuxt/store/i18n.js'

function parse(preference) {
  if(typeof preference == 'string') {
    return preference.split(';')[0].split(',');
  }

  return preference;
};

function lang(locale) {
  var m = locale.match(/^([^-]+)(?:-.*)?$/);
  if (m) { return m[1]; }
  return locale;
};

function bestLanguage(preference, options) {
  var preferences = parse(preference);
  var backup = null;

  for(var i in preferences) {
    var p = preferences[i];

    // compare fully:
    var choice = options.find((option) => (option).toLowerCase() == p);

    if (choice) {
      return choice;
    }

    // compare just language (not region)
    choice = options.find((option) => lang(option) == lang(p));
    if (choice) {
      backup = backup || choice;
    }
  }

  return backup || options[0];
};

Vue.use(VueI18n);

export default async ({ app, store, req }) => {
  if(store) {
    store.registerModule('i18n',{...i18nModule} ,{preserveState: !req});
  } else {
    console.error("nuxt i18n requires vuex store - create store/index.js");
  }

  const { i18n } = store.state;
  let currentLocale = 'en';

  
  if(req) {
    const acceptLanguage = req.headers['accept-language'];
    const supportedLocales = await store.dispatch('i18n/preload');
    currentLocale = bestLanguage(acceptLanguage, supportedLocales);
    store.commit('i18n/SET_LANG', currentLocale);
  } else {
    currentLocale = i18n.currentLocale;
  }

  const localeMessages = await store.dispatch('i18n/getLocales');

  const options = {
    locale: currentLocale,
    fallbackLocale: 'en',
    messages: localeMessages,
    dateTimeFormats: {
      'en': {
        short: {
          year: 'numeric', month: 'short', day: 'numeric'
        },
        long: {
          year: 'numeric', month: 'short', day: 'numeric',
          weekday: 'short', hour: 'numeric', minute: 'numeric'
        }
      },
    },
  };

  app.i18n = new VueI18n(options);


}

