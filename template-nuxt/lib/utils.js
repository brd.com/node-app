export const pause = (delay) => new Promise(res => setTimeout(res,delay));

export function get(string) {
  const parts = string.split('.');
  return function() {
    return parts.reduce((object,key) => object&&object[key],this)
  }
}

export function PromiseLatch() {
  this._res = [];
  this._rej = [];
}

PromiseLatch.prototype = {
  promise: function() {
    return new Promise((res,rej) => {
      this._res.push(res);
      this._rej.push(rej);
    });
  },
  resolve: function(...args) {
    if (this._res) {
      var call = this._res;
      this._rej = [];
      this._res = [];

      call.forEach((c) => c(...args));
    }
  },
  reject: function() {
    var args = Array.prototype.slice.call(arguments);
    if (this._rej) {
      var call = this._rej;
      this._rej = [];
      this._res = [];

      call.forEach((c) => c.apply(this,args));
    }
  },
}

export function inheritProp(name) {
  return {
    inherited: true,
    get: function() {
      var p = this.$parent;
      while (p) {
        // if it has something, and it's not a computed property:
        if (name in p && !(p.$options.computed && name in p.$options.computed && p.$options.computed[name].inherited == true)) {
          return p[name];
        }

        p = p.$parent;
      }

      return null;
    },
    set: function(value) {
      var p = this.$parent;

      while (p) {
        if (name in p && !(p.$options.computed && name in p.$options.computed && p.$options.computed[name].inherited == true)) {
          return p[name] = value;
        }

        p = p.$parent;
      }

      return null;
    },
  }
};
