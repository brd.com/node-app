# @brd.com/node-app

Node-app tries to abstract the annoying parts of node app development, namely:

- Hot reload in development
- Reasonable clustering behavior in production
- Automatic request ID logging and handling (defers to CF-Ray header if it exists)
- Sentry integration
- Internationalization
- Pushing resource timing to google cloud tracing
- More to come!

## Getting Started

Create an app named app-name:

```
npx @brd.com/node-app create app-name
```

This will spit out a template that doesn't really do much, but you can
run it in development:

```node-app dev```

Or in production

```node-app start```

You can also create a nuxt-based application using:

```
npx @brd.com/node-app create-nuxt app-name
```

