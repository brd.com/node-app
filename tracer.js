let tracer;

if(process.env.GCLOUD_PROJECT_ID) {
  try {
    tracer = require('@google-cloud/trace-agent').start({
      projectId: process.env.GCLOUD_PROJECT_ID,
    });
  } catch(x) {
    console.error("Error enabling trace agent: ",x);
  }
}

if(!tracer) {
  tracer = {
    createChildSpan() {
      return {
        endSpan: () => null,
      }
    }
  };
}

module.exports = tracer;
