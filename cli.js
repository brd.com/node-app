#!/usr/bin/env node

const path = require('path');
const fs = require('fs');
const child_process = require('child_process');
const template = require('./tools/template');

function mainFile(file) {
  if(file) {
    return path.resolve(process.cwd(),file);
  } else {
    try {
      var packageFile = path.resolve(process.cwd(),'package.json');
      var nuxtConfigFile = path.resolve(process.cwd(),'nuxt.config.js');
      if(fs.existsSync(nuxtConfigFile)) return nuxtConfigFile;
      
      var config = JSON.parse(fs.readFileSync(packageFile));
      var mainFile = config.main || 'server';

      return path.resolve(process.cwd(),mainFile);
    } catch(x) {
      console.error("ERROR Finding Main File ",x);
      throw x;
    }
  }

}

const operations = {
  create: (name,location) => {
    if(!name) {
      console.log("Specify an app name");
      sys.exit(1);
    }

    if(!name) {
      console.log("Specify a location");
      sys.exit(1);
    }

    console.log(`CREATE ${name} in ${location}`);

    var packageDef = JSON.parse(fs.readFileSync(path.resolve(__dirname,'package.json')))
    moduleVersion = `^${packageDef.version}`;

    if(name.match(/^example/) && location.match(/^example/) && process.cwd() == __dirname) {
      console.log("Spitting out example, using '../' for module version.");
      moduleVersion = '../';
    }

    const dest = (location && path.resolve(process.cwd(),location)) || process.cwd();

    const tmplConfig = {
      __PACKAGE__: name.toLowerCase(),
      __NODE_APP_MODULE_VERSION__: moduleVersion,
    };
    
    template(path.resolve(__dirname,'template-node'),dest,tmplConfig);

    template(path.resolve(__dirname,'node_modules/@brd.com/deploy-it/template-gitlab-ci.yml'),
             path.join(dest,'.gitlab-ci.yml'),
             tmplConfig
            );

    child_process.spawnSync('npm',['i'],{ cwd: dest, stdio: 'inherit' });
  },

  'create-nuxt': (name,location) => {
    operations.create.call(operations,name,location);

    const dest = (location && path.resolve(process.cwd(),location)) || process.cwd();
    
    template(path.resolve(__dirname,'template-nuxt'),dest, {
      __PACKAGE__: name.toLowerCase(),
    });

    child_process.spawnSync('npm',['i','--save','nuxt','vue-i18n','@nuxtjs/sentry','@brd.com/butter-vue'],{ cwd: dest, stdio: 'inherit' });
    child_process.spawnSync('npm',['i','--save-dev','node-sass','sass-loader'],{ cwd: dest, stdio: 'inherit' });

    const pkgfile = path.resolve(process.cwd(),location,'package.json');
    const pkg = JSON.parse(fs.readFileSync(pkgfile));
    pkg.scripts.dev = 'nuxt dev';
    fs.writeFileSync(pkgfile,JSON.stringify(pkg,null,2));
  },


  dev(file) {
    var devtool = require('./tools/development');
    var file = mainFile(file);
    
    devtool({
      file: file ,
      port: process.env.PORT || 8081,
      host: process.env.HOST || '0.0.0.0'
    });
  },

  start() {
    const cluster = require('./tools/cluster')
    var file = mainFile(file);
    
    cluster.start({
      file: file,
      dev: false,
      workers: process.env.WORKERS || 1,
      workerMaxRequests: process.env.WORKER_MAX_REQUESTS || 10000,
      workerMaxLifetime: process.env.WORKER_MAX_LIFETIME || 60000,
      address: process.env.HOST || '0.0.0.0',
      port: process.env.PORT || '80',
    });
  },
}

var op = process.argv[2];
var args = process.argv.slice(3);

if(operations[op]) operations[op].apply(this,args);
else {
  console.log("unknown operation: ",op);
  console.log(" try - create <partner-name>");
}
